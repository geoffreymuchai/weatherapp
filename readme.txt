# Weatherapp README #

### Assumptions made ###
- The `1HrP` column will never have data and therefore can be removed from the column list
- The Maximum and Minimum temperatures will always be integer values
- The aggregate row is a mixture of summations and averages and so the values are not relevant
for the calculations required for spread analysis

### Running the Application ###
To run the application execute the following command:
`python weather.py`

### Running tests ###
To run the test suite execute the following command:
`python weather_tests.py`