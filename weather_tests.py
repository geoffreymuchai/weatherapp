import unittest
from weather import (
    process_weather_lines,
    convert_weather_database_to_data_map,
    find_maximum_spread)


class TestWeatherUtils(unittest.TestCase):

    def test_process_weather_lines_with_column_fields(self):
        weather_dat_raw_columns = (
            " Dy MxT   MnT   AvT   HDDay  AvDP 1HrP "
            "TPcpn WxType PDir AvSp Dir MxS SkyC MxR MnR AvSLP")
        weather_dat_processed_columns = [
            "Dy", "MxT", "MnT", "AvT", "HDDay", "AvDP", "1HrP", "TPcpn",
            "WxType", "PDir", "AvSp", "Dir", "MxS", "SkyC", "MxR", "MnR",
            "AvSLP"]
        self.assertListEqual(
            process_weather_lines(weather_dat_raw_columns),
            weather_dat_processed_columns)

    def test_process_weather_lines_without_hdday_or_wxtype(self):
        row_without_hdday_or_wxtype = (
            "   2  79    63    71          46.5       0.00         330  8.7"
            " 340  23  3.3  70 28 1004.5")
        expected_row_list = [
            "2", "79", "63", "71", "", "46.5", "0.00", "", "330", "8.7", "340",
            "23", "3.3", "70", "28", "1004.5"]

        self.assertListEqual(
            process_weather_lines(row_without_hdday_or_wxtype),
            expected_row_list)

    def test_process_weather_lines_with_hdday_but_no_wxtype(self):
        row_with_hdday_alone = (
            " 9  86    32*   59       6  61.5       0.00         240  7.6 220"
            "  12  6.0  78 46 1018.6")
        expected_row_list = [
            "9", "86", "32", "59", "6", "61.5", "0.00", "", "240", "7.6",
            "220", "12", "6.0", "78", "46", "1018.6"]

        self.assertListEqual(
            process_weather_lines(row_with_hdday_alone),
            expected_row_list)

    def test_process_weather_lines_with_wxtype_but_no_hdday(self):
        row_with_wxtype_alone = (
            "  28  84    68    76          65.6       0.00 RTFH    280  7.6"
            " 340  16  7.0 100 51 1011.0")
        expected_row_list = [
            "28", "84", "68", "76", "", "65.6", "0.00", "RTFH", "280", "7.6",
            "340", "16", "7.0", "100", "51", "1011.0"
        ]

        self.assertListEqual(
            process_weather_lines(row_with_wxtype_alone),
            expected_row_list)

    def test_convert_weather_database_to_data_map(self):
        weather_database = [
            ["Dy", "MxT", "MnT", "AvT", "HDDay", "AvDP", "1HrP", "TPcpn",
             "WxType", "PDir", "AvSp", "Dir", "MxS", "SkyC", "MxR", "MnR",
             "AvSLP"],
            [''],
            ["9", "86", "32", "59", "6", "61.5", "0.00", "", "240", "7.6",
             "220", "12", "6.0", "78", "46", "1018.6"],
            ["2", "79", "63", "71", "", "46.5", "0.00", "", "330", "8.7",
             "340", "23", "3.3", "70", "28", "1004.5"],
            ["28", "84", "68", "76", "", "65.6", "0.00", "RTFH", "280", "7.6",
             "340", "16", "7.0", "100", "51", "1011.0"]]
        datamap = convert_weather_database_to_data_map(weather_database)

        self.assertEqual(len(datamap['Dy']), 3)

    def test_find_maximum_spread(self):
        datamap = {
            "Dy": ["2", "9", "28"],
            "MxT": ["79", "86", "84"],
            "MnT": ["63", "32", "68"]}
        (day_of_max_spread, max_spread) = find_maximum_spread(datamap)

        self.assertEqual(day_of_max_spread, "9")
        self.assertEqual(max_spread, 54)


if __name__ == '__main__':
    unittest.main()
