import re
from collections import defaultdict

REGEX_SUB_PATTERN = '( {7}|[^a-zA-Z0-9.]+)'
NO_DATA_COLUMN = '1HrP'
MAXIMUM_TEMP_KEY = 'MxT'
MINIMUM_TEMP_KEY = 'MnT'
DATE_KEY = 'Dy'


def process_weather_lines(line):
    stripped_line = line.strip()
    parsed_line = re.sub(REGEX_SUB_PATTERN, ',', stripped_line)
    return parsed_line.split(',')


def read_weather_database():
    weather_database = []

    with open('weather.dat', 'r') as weather_file:
        for line in weather_file:
            weather_database.append(process_weather_lines(line))

    return weather_database


def convert_weather_database_to_data_map(weather_database):
    columns = weather_database[0]
    # Remove the column without data so that data is mapped correctly
    columns.remove(NO_DATA_COLUMN)

    # Remove empty rows and aggregation row
    data_rows = [row for row in weather_database[1:]
                 if len(row) == len(columns)]

    datamap = defaultdict(list)

    [datamap[column].append(row[idx])
        for idx, column in enumerate(columns)
        for row in data_rows]

    return datamap


def reduce_multiple_lists(function, first_list, second_list):
    reduced_list = []

    if len(first_list) != len(second_list):
        raise TypeError('Both lists should be of the same size')
    else:
        reduced_list = [function(item, second_list[index])
                        for index, item in enumerate(first_list)]

    return reduced_list


def find_maximum_spread(datamap):
    max_temp_rows = datamap[MAXIMUM_TEMP_KEY]
    min_temp_rows = datamap[MINIMUM_TEMP_KEY]

    spread_function = lambda x, y: int(x) - int(y)

    spread_list = reduce_multiple_lists(
        spread_function,
        max_temp_rows,
        min_temp_rows)

    max_spread = max(spread_list)
    index_of_max_spread = spread_list.index(max_spread)
    day_of_max_spread = datamap[DATE_KEY][index_of_max_spread]

    return (day_of_max_spread, max_spread)


def process_weather_database():
    weather_database = read_weather_database()
    datamap = convert_weather_database_to_data_map(weather_database)
    (day_of_max_spread, max_spread) = find_maximum_spread(datamap)

    print "Day {} had a maximum spread of {}".format(
        day_of_max_spread, max_spread)

if __name__ == '__main__':
    process_weather_database()
